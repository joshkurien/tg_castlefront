class AddBasicResponses < SeedMigration::Migration
  def up
    Response.create(key: 'welcome', text: 'Welcome')
    Response.create(key: 'spam', text: 'Please dont type random things')
    Response.create(key: 'smart_ass', text: "Think you're smart, huh?")
  end

  def down
    Response.destroy_all
  end
end
