# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2019_07_26_210757) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "beings", force: :cascade do |t|
    t.string "name"
    t.integer "health"
    t.integer "armour", default: 0
    t.integer "attack"
    t.string "type"
    t.string "faction"
    t.integer "mana_cost"
    t.integer "max_health"
    t.integer "state", default: 0
    t.string "unitable_type"
    t.bigint "unitable_id"
    t.jsonb "effects", default: {}, null: false
    t.jsonb "details", default: {}, null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["unitable_type", "unitable_id"], name: "index_beings_on_unitable_type_and_unitable_id"
  end

  create_table "card_states", force: :cascade do |t|
    t.bigint "player_id"
    t.string "type"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["player_id"], name: "index_card_states_on_player_id"
  end

  create_table "field_positions", force: :cascade do |t|
    t.bigint "field_id"
    t.integer "position"
    t.integer "attack_direction"
    t.string "type"
    t.boolean "occupied", default: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "lock_version", default: 0
    t.index ["field_id"], name: "index_field_positions_on_field_id"
  end

  create_table "fields", force: :cascade do |t|
    t.bigint "game_id"
    t.integer "top_position"
    t.integer "bottom_position"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["game_id"], name: "index_fields_on_game_id"
  end

  create_table "games", force: :cascade do |t|
    t.integer "winner"
    t.integer "status", default: 0
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "players", force: :cascade do |t|
    t.bigint "user_id"
    t.bigint "game_id"
    t.integer "mana"
    t.integer "coins"
    t.string "type"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "health"
    t.integer "info_message_id"
    t.integer "field_message_id"
    t.integer "action_message_id"
    t.integer "status", default: 0
    t.index ["game_id"], name: "index_players_on_game_id"
    t.index ["user_id"], name: "index_players_on_user_id"
  end

  create_table "responses", force: :cascade do |t|
    t.integer "key"
    t.string "text"
    t.index ["key"], name: "index_responses_on_key"
  end

  create_table "seed_migration_data_migrations", id: :serial, force: :cascade do |t|
    t.string "version"
    t.integer "runtime"
    t.datetime "migrated_on"
  end

  create_table "users", force: :cascade do |t|
    t.integer "telegram_id"
    t.string "username"
    t.string "first_name"
    t.string "last_name"
    t.boolean "is_bot"
    t.string "language"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

end
