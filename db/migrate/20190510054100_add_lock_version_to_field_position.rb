class AddLockVersionToFieldPosition < ActiveRecord::Migration[5.2]
  def change
    add_column :field_positions, :lock_version, :integer, default: 0
  end
end
