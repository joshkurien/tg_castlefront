class CreateFieldPositions < ActiveRecord::Migration[5.2]
  def change
    create_table :field_positions do |t|
      t.references :field
      t.integer :position
      t.integer :attack_direction
      t.string :type
      t.boolean :occupied, default: false

      t.timestamps
    end
  end
end
