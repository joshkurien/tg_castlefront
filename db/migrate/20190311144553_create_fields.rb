class CreateFields < ActiveRecord::Migration[5.2]
  def change
    create_table :fields do |t|
      t.references :game
      t.integer :top_position
      t.integer :bottom_position

      t.timestamps
    end
  end
end
