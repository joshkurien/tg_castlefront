class AddDefaultsToJsonFieldsInBeings < ActiveRecord::Migration[5.2]
  def change
    change_column :beings, :effects, :jsonb, default: {}, null: false
    change_column :beings, :details, :jsonb, default: {}, null: false
  end
end
