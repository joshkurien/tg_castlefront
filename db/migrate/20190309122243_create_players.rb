class CreatePlayers < ActiveRecord::Migration[5.2]
  def change
    create_table :players do |t|
      t.references :user
      t.references :game
      t.integer :mana
      t.integer :coins
      t.string :type

      t.timestamps
    end
  end
end
