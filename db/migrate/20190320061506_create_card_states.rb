class CreateCardStates < ActiveRecord::Migration[5.2]
  def change
    create_table :card_states do |t|
      t.references :player
      t.string :type

      t.timestamps
    end
  end
end
