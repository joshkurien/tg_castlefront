class AddAttributesToPlayer < ActiveRecord::Migration[5.2]
  def change
    add_column :players, :health, :integer
    add_column :players, :info_message_id, :integer
    add_column :players, :field_message_id, :integer
    add_column :players, :action_message_id, :integer
    add_column :players, :status, :integer, default: 0
  end
end
