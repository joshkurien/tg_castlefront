class CreateBeings < ActiveRecord::Migration[5.2]
  def change
    create_table :beings do |t|
      t.string :name
      t.integer :health
      t.integer :armour, default: 0
      t.integer :attack
      t.string :type
      t.string :faction
      t.integer :mana_cost
      t.integer :max_health
      t.integer :state, default: 0
      t.references :unitable, polymorphic: true, index: true

      t.jsonb :effects
      t.jsonb :details
      t.timestamps
    end
  end
end
