require 'rails_helper'

RSpec.describe States::FieldPosition, type: :model do
  describe '#occupy' do
    let(:u1) { FactoryBot.create(:user) }
    let(:u2) { FactoryBot.create(:user) }
    let(:game) { Game.new.init_game(u1, u2) }
    let(:field) { game.field }
    let(:field_position) { field.p1_field_positions.where(position: 0).take }

    it 'should mark field position as occupied' do
      expect(field_position.occupied?).to be_falsey
      field_position.occupy
      expect(field_position.occupied?).to be_truthy
    end

    it 'should mark opposite field to attack at this one' do
      opposite_position = field.p2_field_positions.where(position: 0).take
      opposite_position.up!
      opposite_position.occupy
      expect{ field_position.occupy }.to change{ opposite_position.reload.straight? }.from(false).to(true)
    end

    it 'should add a position to the top or bottom if topmost and/or bottom' do
      expect(field.top_position).to be(field_position.position)
      expect { field_position.occupy }.to change{ field.p1_field_positions.count }.from(1).to(3)
      p1_field_positions = field.p1_field_positions
      expect(p1_field_positions.first.position).to eq(field_position.position - 1)
      expect(p1_field_positions.last.position).to eq(field_position.position + 1)
    end

    it 'should add a position to the top and bottom for opponent side as well' do
      expect(field.top_position).to be(field_position.position)
      expect(field.bottom_position).to be(field_position.position)
      expect {field_position.occupy}.to change {field.p2_field_positions.count}.from(1).to(3)
      p1_field_positions = field.p2_field_positions
      expect(p1_field_positions.first.position).to eq(field_position.position - 1)
      expect(p1_field_positions.last.position).to eq(field_position.position + 1)
    end

    it 'should not change anything if there is already positions above and below' do
      field.add_top_position
      field.add_bottom_position
      expect {field_position.occupy}.to_not change {field.p2_field_positions.count}
    end
  end
end
