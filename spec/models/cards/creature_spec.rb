require 'rails_helper'

RSpec.describe Cards::Creature, type: :model do
  describe '.send_info' do
    it "should send spam if invalid code was sent" do
      chat_id = '1234'
      random_spam = 'stop spamming'
      expect_any_instance_of(Response).to receive(:get_random_text).with(Response.keys[:spam]).and_return(random_spam)
      expect(TelegramClient).to receive(:send_message).with(chat_id, random_spam)

      Cards::Creature.send_info('xyz', chat_id)
    end

    it "should send the information of the correct creature if valid" do
      chat_id = '1234'
      description = 'Some description'
      creature_hash = {:name => "Mygdillion",
                       :health => 7,
                       :attack => 3,
                       :armour => 0,
                       :deck => :blue,
                       :mana_cost => 4}
      all_creatures = {bc1: creature_hash.merge(description: description)}
      creature = Cards::Creature.new
      display_text = 'Some text'

      expect_any_instance_of(Constants::Mappings).to receive(:all_creatures).and_return(all_creatures)
      expect(Cards::Creature).to receive(:new).with(creature_hash).and_return(creature)
      expect(creature).to receive(:info_display).with(description).and_return(display_text)
      expect(TelegramClient).to receive(:send_message).with(chat_id, display_text)

      Cards::Creature.send_info('bc1', chat_id)
    end
  end
end
