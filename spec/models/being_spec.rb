require 'rails_helper'

RSpec.describe Cards::Being, type: :model do
  let(:u1) {FactoryBot.create(:user)}
  let(:u2) {FactoryBot.create(:user)}
  let(:game) {Game.new.init_game(u1, u2)}
  let(:field) {game.field}

  describe '#deploy' do
    it 'should expect an error if card is not in hand' do
      being = FactoryBot.create(:being, unitable: game.player_one.deck)
      expect {(being.deploy(0))}.to raise_exception(GameErrors::CardNotInHandError)
    end

    it 'should deploy card to the right position if player one' do
      field_position = field.p1_field_positions.where(position: 0).take
      being = FactoryBot.create(:being, unitable: game.player_one.hand)

      expect {being.deploy(0)}.to change {field.p1_field_positions.count}.from(1).to(3)
      expect(being.unitable).to eq(field_position.reload)
      expect(being.in_field?).to be_truthy
    end

    it 'should deploy card to the right position if player two' do
      field_position = field.p2_field_positions.where(position: 0).take
      being = FactoryBot.create(:being, unitable: game.player_two.hand)

      expect {being.deploy(0)}.to change {field.p2_field_positions.count}.from(1).to(3)
      expect(being.unitable).to eq(field_position.reload)
      expect(being.in_field?).to be_truthy
    end

    it 'should raise an error if position does not exist' do
      being = FactoryBot.create(:being, unitable: game.player_one.hand)

      expect {(being.deploy(4))}.to raise_exception(GameErrors::InvalidDeployError, 'Attempting to deploy to non existent position')
    end

    it 'should not allow deployment to an already occupied position' do
      being_one = FactoryBot.create(:being, unitable: game.player_one.hand)
      being_two = FactoryBot.create(:being, unitable: game.player_one.hand)

      expect {being_one.deploy(0)}.to change {being_one.in_field?}.from(false).to(true)

      expect {being_two.deploy(0)}.to raise_exception(GameErrors::FieldPositionOccupiedError)
    end
  end

  describe '#disarmed?' do
    let(:being) {FactoryBot.create(:being, unitable: game.player_one.hand)}

    it 'should return false if unit is not disarmed' do
      expect(being.disarmed?).to be_falsey
    end

    it 'should return a true value if unit is disarmed' do
      being.effects[Cards::Being::EFFECTS[:disarmed]] = 1
      being.save!
      expect(being.reload.disarmed?).to be_truthy
    end
  end

  describe '#silenced?' do
    let(:being) {FactoryBot.create(:being, unitable: game.player_one.hand)}

    it 'should return false if unit is not silenced' do
      expect(being.silenced?).to be_falsey
    end

    it 'should return a true value if unit is silenced' do
      being.effects[Cards::Being::EFFECTS[:silenced]] = 1
      being.save!
      expect(being.reload.silenced?).to be_truthy
    end
  end
end
