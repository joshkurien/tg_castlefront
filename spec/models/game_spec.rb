require 'rails_helper'

RSpec.describe Game, type: :model do
  describe '#init_game' do
    let(:u1) {FactoryBot.create(:user)}
    let(:u2) {FactoryBot.create(:user)}

    it 'should create 2 players' do
      game = Game.new.init_game(u1, u2)
      player_1 = game.player_one
      player_2 = game.player_two

      expect(player_1).to_not be_nil
      expect(player_2).to_not be_nil
      expect(game.active?).to be_truthy
    end

    it 'should create 2 decks' do
      game = Game.new.init_game(u1, u2)
      player_1 = game.player_one
      player_2 = game.player_two

      deck_one = player_1.deck
      deck_two = player_2.deck

      expect(deck_one).to_not be_nil
      expect(deck_two).to_not be_nil
    end

    it 'should create 2 graves' do
      game = Game.new.init_game(u1, u2)
      player_1 = game.player_one
      player_2 = game.player_two

      grave_one = player_1.grave
      grave_two = player_2.grave

      expect(grave_one).to_not be_nil
      expect(grave_two).to_not be_nil
    end

    it 'should create 2 hands' do
      game = Game.new.init_game(u1, u2)
      player_1 = game.player_one
      player_2 = game.player_two

      hand_one = player_1.hand
      hand_two = player_2.hand

      expect(hand_one).to_not be_nil
      expect(hand_two).to_not be_nil
    end

    it 'should create a field' do
      game = Game.new.init_game(u1, u2)
      field = game.field
      expect(field).to be_an_instance_of(Field)
      expect(field.top_position).to be(0)
      expect(field.bottom_position).to be(0)
    end

    it 'should create 2 field positions' do
      game = Game.new.init_game(u1, u2)
      field = game.field
      p1_positions = field.p1_field_positions
      p2_positions = field.p2_field_positions

      expect(p1_positions.size).to be(1)
      expect(p2_positions.size).to be(1)

      expect(p1_positions.first.position).to be(0)
      expect(p2_positions.first.position).to be(0)
    end
  end
end
