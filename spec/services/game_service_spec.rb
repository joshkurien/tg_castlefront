require 'rails_helper'

RSpec.describe GameService do

  describe '#expected_loss' do
    let(:attack_val) {3} # this is the default attack value in factory

    let(:game) {Game.new.init_game(FactoryBot.create(:user), FactoryBot.create(:user))}
    # setting up a turn for tests
    before do
      b1 = FactoryBot.create(:being, unitable: game.player_one.hand)
      b2 = FactoryBot.create(:being, unitable: game.player_two.hand)
      b3 = FactoryBot.create(:being, unitable: game.player_one.hand)
      b4 = FactoryBot.create(:being, unitable: game.player_two.hand)
      b5 = FactoryBot.create(:being, unitable: game.player_two.hand)
      b1.deploy(0)
      game.field.p1_field_positions[1].attack_direction = States::FieldPosition.attack_directions[:straight]
      game.field.p1_field_positions[1].save!
      b2.deploy(0)
      game.field.p2_field_positions[1].attack_direction = States::FieldPosition.attack_directions[:straight]
      game.field.p2_field_positions[1].save!
      b3.deploy(1)
      game.field.p1_field_positions.reload[2].attack_direction = States::FieldPosition.attack_directions[:up]
      game.field.p1_field_positions[2].save!
      b4.deploy(1)
      game.field.p2_field_positions.reload[2].attack_direction = States::FieldPosition.attack_directions[:down]
      game.field.p2_field_positions[2].save!
      b5.deploy(2)
      game.field.p2_field_positions[3].attack_direction = States::FieldPosition.attack_directions[:up]
      game.field.p2_field_positions[3].save!
    end

    context '' do
      it 'should store the correct value of expected point loss for each unit' do
        p1_loss, p2_loss = GameService.new.expected_loss(game)

        expect(p1_loss).to eq(attack_val * 2)
        expect(p2_loss).to eq(attack_val)
      end
    end
  end
end