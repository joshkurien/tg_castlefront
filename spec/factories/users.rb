FactoryBot.define do
  factory :user do
    first_name {Faker::FunnyName.name}
    telegram_id {159911854}
    is_bot {false}
  end
end