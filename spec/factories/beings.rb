FactoryBot.define do
  factory :being, class: Cards::Being do |f|
    f.name { Faker::Movies::LordOfTheRings.character }
    f.health { 10 }
    f.mana_cost { Faker::Number.digit }
    f.attack { 3 }
    f.faction { :blue }
  end
end
