class GameService
  def end_turn(game)
    field = game.field

  end

  def expected_loss(game)
    p1_positions = game.field.p1_field_positions
    p2_positions = game.field.p2_field_positions
    p1_loss = 0
    p2_loss = 0

    p2_positions.each do |p2_pos|
      if p2_pos.occupied?
        p2_pos.unit[Cards::Being::DETAILS[:expected_loss]] = 0
      end
    end

    end
    p1_positions.each_with_index do |p1_pos, i|
      if p1_pos.occupied?
        p1_pos.unit[Cards::Being::DETAILS[:expected_loss]] = 0

        if !p1_pos.unit.disarmed?
          p2_pos = p2_positions[i + States::FieldPosition.attack_directions[p1_pos.attack_direction]]
          if p2_pos.occupied?
            p2_pos.unit[Cards::Being::DETAILS[:expected_loss]] += p1_pos.unit.attack
          else
            p2_loss += p1_pos.unit.attack
          end
        end
      end
    end

    p2_positions.each_with_index do |p2_pos, i|
      if p2_pos.occupied? && !p2_pos.unit.disarmed?
        p1_pos = p1_positions[i + States::FieldPosition.attack_directions[p2_pos.attack_direction]]
        if p1_pos.occupied?
          p1_pos.unit[Cards::Being::DETAILS[:expected_loss]] += p2_pos.unit.attack
        else
          p1_loss += p2_pos.unit.attack
        end
      end
    end

    p2_positions.each do |p2_pos|
      if p2_pos.occupied?
        p2_pos.unit.save!
      end
    end

    p1_positions.each do |p1_pos|
      if p1_pos.occupied?
        p1_pos.unit.save!
      end
    end

    return p1_loss, p2_loss
  end
end