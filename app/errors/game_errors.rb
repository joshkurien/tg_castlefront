module GameErrors
  class FieldPositionOccupiedError < StandardError
    def initialize(msg = 'Attempting to deploy to occupied position')
      super
    end
  end

  class InvalidDeployError < StandardError
    def initialize(msg = 'Attempting to deploy to non existent position')
      super
    end
  end

  class CardNotInHandError < StandardError
    def initialize(msg = 'Attempting to deploy without card being in hand!')
      super
    end
  end
end