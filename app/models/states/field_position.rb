module States
  class FieldPosition < ApplicationRecord
    include OptimisticallyLockable
    validates_presence_of :attack_direction, :position
    belongs_to :field
    has_one :unit, class_name: 'Cards::Being', as: :unitable, dependent: :destroy
    before_validation :set_attack, on: :create

    enum attack_direction: {up: -1, straight: 0, down: 1}

    # Note: put this function in a transaction for whoever is calling
    def occupy
      self.occupied = true
      save!
      opposite_position = opposite_side.where(position: position, field: field).take
      if opposite_position.occupied
        opposite_position.attack_direction = 'straight'
        opposite_position.save
      end

      if position == field.top_position
        field.add_top_position
      end
      if position == field.bottom_position
        field.add_bottom_position
      end
    end

    def inline_buttons
      unless occupied
        return [Constants::Buttons::DEFAULT_FIELD_BUTTON, attack_symbols[attack_direction.to_sym]], "{type: :field_position, position: #{self.position}, unit: nil}"
      end
      return unit.field_message(attack_symbols[attack_direction.to_sym]), "{type: :field_position, position: #{self.position}, unit: #{unit.id}}"
    end

    def set_attack
      self.attack_direction = rand(3) - 1
    end

    def opposite_side
      raise 'Child class should define method'
    end
  end
end