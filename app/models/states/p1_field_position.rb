module States
  class P1FieldPosition < FieldPosition
    def opposite_side
      P2FieldPosition
    end

    def attack_symbols
      {
          up: '↗️',
          straight: '➡️',
          down: '↘️'
      }
    end
  end
end