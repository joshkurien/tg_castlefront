module States
  class P2FieldPosition < FieldPosition
    def opposite_side
      P1FieldPosition
    end

    def attack_symbols
      {
          up: '↖️️',
          straight: '⬅️',
          down: '↙️️'
      }
    end
  end
end