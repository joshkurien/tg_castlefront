class Field < ApplicationRecord
  belongs_to :game
  validates_presence_of :bottom_position, :top_position
  has_many :p1_field_positions, -> {order(:position)}, :class_name => 'States::P1FieldPosition', dependent: :destroy
  has_many :p2_field_positions, -> {order(:position)}, :class_name => 'States::P2FieldPosition', dependent: :destroy

  def tg_message
    combined_fields = p1_field_positions.zip(p2_field_positions)
    p1_name = game.player_one.user.full_name
    p2_name = game.player_two.user.full_name
    message = "#{p1_name}    VS     #{p2_name}\n"

    buttons = []
    lbreak = '------------------'.freeze
    combined_fields.each do |p1_position, p2_position|
      p1_texts, p1_data = p1_position.inline_buttons
      p2_texts, p2_data = p2_position.inline_buttons

      num = p1_texts.count > p2_texts.count ? p1_texts.count : p2_texts.count

      buttons << [{text: lbreak, callback_data: "{}"}, {text: lbreak, callback_data: "{}"}]

      if p1_texts[0] == Constants::Buttons::DEFAULT_FIELD_BUTTON && p2_texts[0] == Constants::Buttons::DEFAULT_FIELD_BUTTON
        buttons << [{text: p1_texts[1], callback_data: p1_data},
                    {text: p2_texts[1], callback_data: p2_data}]
      else
        num.times do |i|
          if p1_texts[i].present?
            p1_button = {text: p1_texts[i], callback_data: p1_data}
          else
            p1_button = {text: Constants::Buttons::DEFAULT_FIELD_BUTTON, callback_data: p1_data}
          end

          if p2_texts[i].present?
            p2_button = {text: p2_texts[i], callback_data: p2_data}
          else
            p2_button = {text: Constants::Buttons::DEFAULT_FIELD_BUTTON, callback_data: p2_data}
          end

          buttons << [p1_button, p2_button]
        end
      end
    end
    return message, buttons
  end

  # Make sure below functions are called inside transactions
  def add_top_position
    self.top_position = top_position + 1
    self.p1_field_positions.create!(position: top_position)
    self.p2_field_positions.create!(position: top_position)
    self.save!
  end

  def add_bottom_position
    self.bottom_position = bottom_position - 1
    self.p1_field_positions.create!(position: bottom_position)
    self.p2_field_positions.create!(position: bottom_position)
    self.save!
  end
end
