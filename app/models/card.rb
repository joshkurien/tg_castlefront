class Card < ApplicationRecord
  validates_presence_of :mana_cost, :state
  enum state: [:in_deck, :in_hand, :in_field, :graveyard]
  self.abstract_class = true
end