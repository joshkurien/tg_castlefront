module OptimisticallyLockable

  def self.included(receiver)
    receiver.lock_optimistically = false
    receiver.class_eval do

      def self.with_optimistic_locking
        original_lock = self.lock_optimistically
        self.lock_optimistically = true

        begin
          yield
        ensure
          self.lock_optimistically = original_lock
        end
      end

    end
  end

end