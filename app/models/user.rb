class User < ApplicationRecord
  enum status: [:created]
  has_many :games, ->(user) {unscope(:where).where("player_one_id = :id OR player_two_id = :id", id: user.id)}, dependent: :destroy
  has_one :player

  def self.get_user(from_params)
    user = find_by_telegram_id(from_params[:id].to_i)
    return user unless user.nil?

    register_user(from_params)
  end


  def full_name
    if last_name.blank?
      return first_name
    end
    first_name + ' ' + last_name
  end

  private

  def self.register_user(info)
    User.create(username: info[:username], telegram_id: info[:id],
                first_name: info[:first_name], last_name: info[:last_name],
                is_bot: info[:is_bot], language: info[:language_code])
  end

end