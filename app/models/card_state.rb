class CardState < ApplicationRecord
  belongs_to :player

  has_many :units, class_name: 'Cards::Being', as: :unitable, dependent: :destroy
end
