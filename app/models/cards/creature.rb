module Cards
  class Creature < Being

    def display
      "#{Constants::Mappings::SYMBOLS[:mana]}#{mana_cost}        #{Constants::Mappings::SYMBOLS[:creature]}" +
          super
    end

    def self.send_info(creature_code, chat_id)
      all_creature_hash = Constants::Mappings.new.all_creatures
      creature_hash = all_creature_hash[creature_code.to_sym]
      if creature_hash.blank?
        TelegramClient.send_message(chat_id, Response.new.get_random_text(Response.keys[:spam]))
        return
      end
      creature = Creature.new(creature_hash.except(:description))

      TelegramClient.send_message(chat_id, creature.info_display(creature_hash[:description]))
    end

    def help(chat_id, help_topic)
      case help_topic.to_sym
      when :help
        TelegramClient.send_message(chat_id, help_all)
        return
      when :blue
        list = Constants::Blue::Creatures::LIST
      when :all
        list = Constants::Mappings.new.all_creatures
      else
        TelegramClient.send_message(chat_id, Response.new.get_random_text(Response.keys[:spam]))
        return
      end
      message = 'Choose a creature to view details of:'
      list.each do |key, value|
        message = message + "\n/#{key.to_s} #{value[:name]}"
      end
      TelegramClient.send_message(chat_id, message)
    end

    private

    def help_all
      'Creeps or creatures help you out in the battlefield, ' \
      'most are grunts who will fight and die for you; and some ' \
      "may have special ways to affect the battlefield.\n\n" \
      'Each creature is from a particular faction and can' \
      'only be summoned if a hero from that faction is on the field' \
      "\n\nTo see the list of creatures from each faction:\n" \
      "/creep_blue \n" \
      'and if you want to really see all the creatures together go for /creep_all'
    end

  end
end