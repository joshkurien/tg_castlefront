module Cards
  class Being < Card
    validates_presence_of :name, :health, :attack, :faction
    before_create :set_init_max_health
    belongs_to :unitable, polymorphic: true

    EFFECTS = {
        disarmed: 'disarmed',
        silenced: 'silenced'
    }

    DETAILS = {
        expected_loss: 'expected_loss'
    }

    def display
      deck_symbol = Constants::Mappings::SYMBOLS[faction.to_sym]
      "#{deck_symbol}\n\n" +
          "#{name}".rjust(15) +
          "\n\n#{current_stats}"
    end

    def info_display(description)
      display + "\n\n" + description
    end

    def deploy(position)
      positions = nil
      field = unitable.player.game.field

      unless (unitable.class == States::Hand)
        raise GameErrors::CardNotInHandError
      end
      case unitable.player
      when PlayerOne
        positions = field.p1_field_positions
      when PlayerTwo
        positions = field.p2_field_positions
      end

      field_position = positions.where(position: position).take
      if field_position.blank?
        raise GameErrors::InvalidDeployError
      end
      if field_position.occupied
        raise GameErrors::FieldPositionOccupiedError
      end

      self.transaction do
        self.unitable = field_position
        self.in_field!
        field_position.class.with_optimistic_locking do
          field_position.occupy
        end
      end
    end

    def field_message(attack)
      message = [current_stats]
      case unitable
      when States::P1FieldPosition
        message << "#{name} #{attack}"
      when States::P2FieldPosition
        message << "#{attack} #{name}"
      end
      message
    end

    def disarmed?
      if self.effects[EFFECTS[:disarmed]]
        return true
      end
      false
    end

    def silenced?
      if self.effects[EFFECTS[:silenced]]
        return true
      end
      false
    end

    private

    def health_display
      if max_health.blank? || max_health == health
        return health.to_s
      end
      "#{health}/#{max_health}"
    end

    def set_init_max_health
      self.max_health = self.health
    end

    def current_stats
      "⚔️#{attack}🛡#{armour}❣️#{health_display} "
    end
  end
end