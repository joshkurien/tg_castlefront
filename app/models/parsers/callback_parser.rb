module Parsers
  class CallbackParser
    def self.process(callback)
      user = User.get_user(callback[:message][:chat])
      begin
        msge, btns = Field.first.tg_message
        TelegramClient.edit_inline_message(user.telegram_id,
                                           callback[:message][:message_id],
                                           msge, btns)
      rescue
        TelegramClient.send_message(user.telegram_id, "Calm down please")
      end
    end
  end
end