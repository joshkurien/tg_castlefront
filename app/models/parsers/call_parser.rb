module Parsers
  class CallParser
    def self.process(message)
      user = User.get_user(message[:from])

      case message[:text]
      when /^\/[b]c\d+$/
        Cards::Creature.send_info(message[:text][1..-1], user.telegram_id)
      when '/start'
        TelegramClient.send_message(user.telegram_id,
                                    (Response.new.get_random_text(Response.keys[:welcome])))
      when '/help'
        TelegramClient.send_message(user.telegram_id,
                                    Response.new.help)
      when /^\/hero_[a-z]+$/
      when /^\/creep_[a-z]+$/
        Cards::Creature.new.help(user.telegram_id, message[:text][7..-1])
      when /^\/spell_[a-z]+$/
      when '/qwe'
        user.player.message_refresh
      else
        default_response(message[:chat][:id])
      end
    end

    private

    def self.default_response(chat)
      TelegramClient.send_message(chat, Response.new.get_random_text(Response.keys[:spam]))
    end
  end
end