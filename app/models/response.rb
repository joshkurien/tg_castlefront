class Response < ActiveRecord::Base
  validates_presence_of :key, :text

  enum key: [:welcome, :spam, :smart_ass]

  def get_random_text(key)
    set = Response.where(key: key)
    set.offset(rand(set.count)).first.text
  end

  def help
    'Hi, this is a game where battles are fought among powerful beings ' \
    'for the cause of defending your castle and destroying the opponents.' \
    "\nThere are heroes and creatures and beings from multiple factions that will come to your aid\n" \
    'For details on the champions: /hero_help' \
    "\nFor details about various spells: /spell_help \n" \
    'For details about the creeps: /creep_help'
  end
end