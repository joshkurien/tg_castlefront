class Player < ApplicationRecord
  belongs_to :game
  belongs_to :user
  validates_presence_of :mana, :coins, :health

  has_one :deck, :class_name => 'States::Deck', dependent: :destroy
  has_one :grave, :class_name => 'States::Grave', dependent: :destroy
  has_one :hand, :class_name => 'States::Hand', dependent: :destroy

  def message_refresh
    user_message = TelegramClient.send_message(user.telegram_id, '```This is space for information about cards```', 'MARKDOWN')
    self.info_message_id = JSON.parse(user_message)["result"]["message_id"]

    cur_game = self.game.advanced_load
    msg, btns = cur_game.field.tg_message
    user_message = TelegramClient.make_inline_buttons(user.telegram_id, msg, btns)
    self.field_message_id = JSON.parse(user_message)["result"]["message_id"]

    self.save!
  end
end
