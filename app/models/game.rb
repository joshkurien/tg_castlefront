class Game < ApplicationRecord
  validates_presence_of :status
  enum status: [:active, :past]
  has_one :field, dependent: :destroy
  has_one :player_one, dependent: :destroy
  has_one :player_two, dependent: :destroy

  def init_game(user1, user2)
    game = nil
    Game.transaction do
      game = Game.create!
      field = Field.create!(game: game, top_position: 0, bottom_position: 0)
      States::P1FieldPosition.create!(field: field, position: 0)
      States::P2FieldPosition.create!(field: field, position: 0)
      p1 = PlayerOne.create!(user: user1, game: game, mana: 0, coins: 0, health: 100)
      p2 = PlayerTwo.create!(user: user2, game: game, mana: 0, coins: 0, health: 100)
      States::Deck.create!(player: p1)
      States::Deck.create!(player: p2)
      States::Hand.create!(player: p1)
      States::Hand.create!(player: p2)
      States::Grave.create!(player: p1)
      States::Grave.create!(player: p2)
    end
    game
  end

  #ToDo Change includes to eager_load if the DB call includes network hops
  def advanced_load
    Game.includes(player_one: [hand: :units, deck: :units, grave: :units],
                  player_two: [hand: :units, deck: :units, grave: :units],
                  field: [p1_field_positions: :unit, p2_field_positions: :unit])
        .find(id)
  end
end
