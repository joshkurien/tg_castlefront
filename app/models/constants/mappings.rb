class Constants::Mappings
  SYMBOLS = {
      mana: '💧',
      creature: '🐯',
      blue: ::Constants::Blue::Defaults::SYMBOL
  }.freeze

  def all_creatures
    creatures = ::Constants::Blue::Creatures::LIST
    return creatures
  end
end