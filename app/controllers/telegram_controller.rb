class TelegramController < ApplicationController
  def call
    if params[:callback_query].present?
      ::Parsers::CallbackParser.process(params[:callback_query])
    elsif params[:edited_message].present?
      ::Parsers::EditParser.new.process(params[:edited_message])
    else
      ::Parsers::CallParser.process(params[:message])
    end
    render status: 204
  end
end